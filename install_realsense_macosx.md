## Intall & Build realsense in Mac OSX

1. Install XCode 6.0+ via the AppStore
2. Install the Homebrew package manager via terminal - [link](http://brew.sh/)
3. Install the following packages via brew:
  * `brew install libusb pkg-config`
  * `brew install homebrew/core/glfw3`
  * `brew install cmake`

**Note** *librealsense* requires CMake version 3.8+ that can be obtained via the [official CMake site](https://cmake.org/download/).  


4. Generate XCode project:
  * `git clone https://github.com/IntelRealSense/librealsense.git`
  * `cd librealsense`
  * `mkdir build && cd build`
  * `sudo xcode-select --reset`
  * `cmake .. -DBUILD_EXAMPLES=true -DBUILD_WITH_OPENMP=false -DHWM_OVER_XU=false -G Xcode` or using `CMake GUI` to build.
5. Open and build the XCode project
  if build Error when using the note below to fix bug.

  <img src="realsense-build.png" width="100%" />


> **Note:** On some Mac systems you might encounter `ld: library not found for -lusb-1.0` error (either in the terminal during make or in XCode) This can be worked-around by setting environment variable: `/bin/launchctl setenv LIBRARY_PATH /usr/local/lib`

## What works?
* SR300, D415 and D435 will st`ream depth, infrared and color at all supported resolutions
* The Viewer, Depth-Quality Tool and most of the examples should work

## What are the known issues?
* Changing configurations will often result in a crash or the new configuration not being applied (WIP)